# Pasos auth google

0. Agregar dependencia [https://pub.dev/packages/google_sign_in]
1. Crear un proyecto en firebase
2. Ingresar a [Participación] y [Authentication]
3. Seleccionar [Google] y habilitar.
4. Ingresar a https://console.developers.google.com/ , entrar con tu cuenta, y seleccionar el proyecto
5. Ingresar a pantalla de consentimiento
6. Darle a editar información
7. Cambiar el nombre de la app y tambien el directorio kotlin
8. En la pantalla principal de firebase darle click a android

9. Corres este para obtener el certificado ' keytool -list -v -alias androiddebugkey -keystore %USERPROFILE%\.android\debug.keystore ' - y lo pegan en certificado de firma (contraseña es android) y click en registrar app
10. Descargas un archivo de google-services.json y lo pegas en > android>app ( alado de build.gradle)
11. Seguir los pasos

12. In ios -Agrega un nuevo app ios en firebase y completar paso 1, 2 (de firebase

13. Agregar la llave reversa de google en - Google Cloud Platform API manager https://console.cloud.google.com/apis/credentials?project=flutter-signin-591fe
14. Ingresar a credenciales
15. Seleccionar la parte de IOS y copiar el 'Esquema de URL de IOs
