import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;

class GoogleSignInService {
  static final GoogleSignIn _googleSignIn = GoogleSignIn(
    scopes: [
      'email',
    ],
  );

  static Future signInWithGoogle() async {
    try {
      //
      final GoogleSignInAccount? account = await _googleSignIn.signIn();
      if (account == null) return;
      final GoogleSignInAuthentication googleKey = await account.authentication;

      final signInWithGoogleEndPoint = Uri(
        scheme: 'https',
        host: "backend-google-signin-app.onrender.com",
        path: '/google',
      );

      final session = await http.post(
        signInWithGoogleEndPoint,
        body: {
          'token': googleKey.idToken,
        },
      );

      print('=======backend=======');

      print(session.body);

      print('========= ID TOKEN =========');
      print(account);
      print(googleKey.idToken);
    } catch (e) {
      print(e);
      return null;
    }
  }

  static Future signOut() async {
    //

    _googleSignIn.signOut();
  }
}
